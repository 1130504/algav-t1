/*area(descri��o,Nome).*/
area(a,'Aldoar,Foz do Douro,Nevogilde').
area(b,'Ramalde').
area(c,'Lordelo do Ouro e Massarelos').
area(d,'Paranhos').
area(e,'Cedofeita').
area(f,'Bonfim').
area(g,'Campanha').

/*subarea(Nome,Area,PosX,PosY).*/

subarea(1,a,20,80).
subarea(2,a,50,30).
subarea(3,b,100,100).
subarea(4,b,90,70).
subarea(5,c,70,50).
subarea(6,c,110,50).
subarea(7,d,150,90).
subarea(8,d,200,90).
subarea(9,e,140,60).
subarea(10,e,160,20).
subarea(11,f,180,60).
subarea(12,f,190,20).
subarea(13,g,230,70).
subarea(14,g,240,30).

/*estrada(CidadeA,CidadeB,Distancia).*/
estrada(1,2,10).
estrada(1,3,100).
estrada(1,4,75).
estrada(1,5,65).
estrada(2,5,30).
estrada(3,4,10).
estrada(3,7,50).
estrada(4,5,25).
estrada(4,6,30).
estrada(4,9,65).
estrada(4,7,70).
estrada(5,6,10).
estrada(6,9,35).
estrada(6,10,60).
estrada(7,8,10).
estrada(7,9,30).
estrada(8,9,70).
estrada(8,11,40).
estrada(8,13,50).
estrada(9,10,10).
estrada(9,11,30).
estrada(10,11,50).
estrada(10,12,40).
estrada(11,12,10).
estrada(11,13,50).
estrada(11,14,60).
estrada(12,14,50).
estrada(13,14,10).

astar(O,D,_,Tp):-
	estimativa(O,D,E),
	astar1([(E,E,0,[O])],D,_,Tp).

astar1([(_,_,Tp,[D|R])|_],D,[D|R],Tp):- reverse([D|R],L3),write('Caminho = '),write(L3).

astar1([(_,_,P,[X|R1])|R2],D,C,Tp):-
	findall((NovaSoma,E1,NP,[Z,X|R1]),(estrada(X,Z,V),
				   not(member(Z,R1)),
				   NP is P+V,
				   estimativa(Z,D,E1),
				   NovaSoma is E1+NP),L),
	append(R2,L,R3),
	sort(R3,R4),
	astar1(R4,D,C,Tp).
	

estimativa(C1,C2,Est):- subarea(C1,_,X1,Y1), subarea(C2,_,X2,Y2), DX is X1-X2,DY is Y1-Y2, Est is sqrt(DX*DX+DY*DY).