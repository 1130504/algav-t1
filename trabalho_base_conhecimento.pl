:- use_module(library(clpfd)).

% AVIAO
distancia_aviao(amerstardam,paris,430).
distancia_aviao(berlin,paris,878).
distancia_aviao(copenhagen,helsinki,884).
distancia_aviao(copenhagen,oslo,484).
distancia_aviao(copenhagen,stockholm,523).
distancia_aviao(dublin,paris,836).
distancia_aviao(helsinki,copenhagen,884).
distancia_aviao(lisbon,madrid,503).
distancia_aviao(london,paris,344).
distancia_aviao(madrid,lisbon,503).
distancia_aviao(madrid,paris,1054).
distancia_aviao(oslo,copenhagen,484).
distancia_aviao(paris,amesterdam,430).
distancia_aviao(paris,berlin,878).
distancia_aviao(paris,dublin,836).
distancia_aviao(paris,london,344).
distancia_aviao(paris,madrid,1054).
distancia_aviao(paris,rome,1107).
distancia_aviao(paris,vienna,1035).
distancia_aviao(rome,paris,1107).
distancia_aviao(stockholm,copenhagen,523).
distancia_aviao(vienna,paris,1035).
% CARRO
distancia_carro(amesterdam,brussels,210).
distancia_carro(amesterdam,paris,501).
distancia_carro(andorra,ljubljana,1430).
distancia_carro(andorra,madrid,609).
distancia_carro(andorra,paris,861).
distancia_carro(berlin,copenhagen,356).
distancia_carro(berlin,paris,1048).
distancia_carro(berlin,prague,351).
distancia_carro(berlin,tallin,1042).
distancia_carro(berlin,warsaw,572).
distancia_carro(bratislava,budapest,200).
distancia_carro(bratislava,vienna,79).
distancia_carro(brussels,amersterdam,210).
distancia_carro(budapest,berlin,356).
distancia_carro(copenhagen,berlin,356).
distancia_carro(copenhagen,oslo,606).
distancia_carro(copenhagen,stockholm,657).
distancia_carro(dublin,paris,1083).
distancia_carro(lisbon,madrid,625).
distancia_carro(lisbon,porto,313).
distancia_carro(ljubljana,andorra,1430).
distancia_carro(ljubljana,vienna,384).
distancia_carro(ljubljana,zagreb,140).
distancia_carro(london,paris,464).
distancia_carro(luxembourg,paris,374).
distancia_carro(madrid,andorra,609).
distancia_carro(madrid,lisbon,625).
distancia_carro(madrid,paris,1276).
distancia_carro(madrid,porto,561).
distancia_carro(oslo,copenhagen,606).
distancia_carro(oslo,stockholm,521).
distancia_carro(paris,amesterdam,501).
distancia_carro(paris,andorra,861).
distancia_carro(paris,berlin,1048).
distancia_carro(paris,brussels,314).
distancia_carro(paris,dublin,1083).
distancia_carro(paris,london,464).
distancia_carro(paris,luxembourg,374).
distancia_carro(paris,madrid,1276).
distancia_carro(paris,rome,1424).
distancia_carro(paris,vienna,1236).
distancia_carro(porto,lisbon,313).
distancia_carro(porto,madrid,561).
distancia_carro(prague,berlin,351).
distancia_carro(prague,vienna,309).
distancia_carro(riga,tallin,309).
distancia_carro(riga,vilnius,302).
distancia_carro(rome,paris,1424).
distancia_carro(tallin,riga,309).
distancia_carro(vienna,bratislava,79).
distancia_carro(vienna,ljubljana,384).
distancia_carro(vienna,paris,1236).
distancia_carro(vienna,prague,309).
distancia_carro(vilnius,riga,302).
distancia_carro(vilnius,warsaw,459).
distancia_carro(warsaw,vilnius,459).
distancia_carro(zabreg,ljubljana,140).

% Descobrir dia da semana
dia(0,domingo).
dia(1,segunda-feira).
dia(2,ter�a-feira).
dia(3,quarta-feira).
dia(4,quinta-feira).
dia(5,sexta-feira).
dia(6,sabado).

mes(1,janeiro).
mes(2,fevereiro).
mes(3,mar�o).
mes(4,abril).
mes(5,maio).
mes(6,junho).
mes(7,julho).
mes(8,agosto).
mes(9,setembro).
mes(10,outubro).
mes(11,novembro).
mes(12,dezembro).


% algoritmo para achar dia da semana
dia_da_semana(Dia,Mes,Ano,DiaDaSemana):-
	Cc is Ano/100,
	Yy is Ano - ((Ano/100)*100),
	C is (Cc/4)-2*Cc-1,
	Y is (5*Yy)/4,
	M is 26*(Mes+1)/10,
	D is mod(floor(C+Y+M+Dia),7),
	dia(D,DiaDaSemana).

% algoritmo para calcular tempo aviao
calc_tempo(C1,C2,Transporte,TempoEmHoras):-
	(Transporte == 'aviao',
	 distancia_aviao(C1,C2,Distancia),
	 Velocidade is 500,!
	;
	distancia_carro(C1,C2,Distancia),
	 Velocidade is 80),!,
	TempoEmHoras is Distancia/Velocidade.

% algoritmo para calcular tempo carro
calc_tempo_aviao(C1,C2,TempoEmHoras):-
	distancia_aviao(C1,C2,Distancia),
	TempoEmHoras is Distancia/500.

% algoritmo para calcular pre�os
preco_aviao(C1,C2,Peso,Preco):-
	distancia_aviao(C1,C2,D),!,
	Preco is D*0.05*Peso.

preco_carro(C1,C2,Peso,Preco):-
	distancia_carro(C1,C2,D),!,
	Preco is D*0.05*Peso.

%Branch and Bound
go(Orig,Dest,Perc):-
	go1([(0,[Orig])],Dest,P),reverse(P,Perc).

go1([(_,Prim)|_],Dest,Prim):- Prim=[Dest|_].

go1([(_,[Dest|_])|Resto],Dest,Perc):- !, go1(Resto,Dest,Perc).

go1([(C,[Ult|T])|Outros],Dest,Perc):-
	findall((NC,[Z,Ult|T]),
		(proximo_no(Ult,T,Z,C1),NC is C+C1),Lista),
	append(Outros,Lista,NPerc),
	sort(NPerc,NPerc1),
	go1(NPerc1,Dest,Perc).

proximo_no(X,T,Z,C):- (distancia_aviao(X,Z,C);distancia_carro(X,Z,C)),
	not(member(Z,T)).

bandb(O,D,C,Tp):- bandb2([(0,[O])],D,C,Tp).
bandb2([(Tp,[D|R])|_],D,[D|R],Tp).
bandb2([(P,[X|R1])|R2],D,C,Tp):-
	findall((Pa,[Z,X|R1]),(estrada(X,Z,V),not(member(Z,R1)),Pa is P+V),L),
	append(R2,L,R3),
	sort(R3,R4),
	bandb2(R4,D,C,Tp).

% A*

